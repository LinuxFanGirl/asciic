/*
       _   , __
    \_|_) /|/  \
      |    | __/
     _|    |   \
    (/\___/|(__/

    Lora Brekke
    CharLib - 13.11.2022
*/
#include <string>
#include <iostream>
#include <stdio.h>

using namespace std;

string drawchar(char letter, int index)
{

switch (toupper(letter))
{
        case 'A':
            switch (index)
            {
               case 0: return " █████ "; break;
               case 1: return "██   ██"; break;
               case 2: return "███████"; break;
               case 3: return "██   ██"; break;
               case 4: return "██   ██"; break;
            }
            break;
        case 'B':
            switch (index)
            {
                case 0: return "██████ "; break;
                case 1: return "██   ██"; break;
                case 2: return "██████ "; break;
                case 3: return "██   ██"; break;
                case 4: return "██████ "; break;
            }
            break;

        case 'C':
            switch (index)
            {
                case 0: return " ██████"; break;
                case 1: return "██     "; break;
                case 2: return "██     "; break;
                case 3: return "██     "; break;
                case 4: return " ██████"; break;
            }
            break;

        case 'D':
            switch (index)
            {
                 case 0: return "██████ "; break;
                 case 1: return "██   ██"; break;
                 case 2: return "██   ██"; break;
                 case 3: return "██   ██"; break;
                 case 4: return "██████ "; break;
            }
            break;

        case 'E':
            switch (index)
            {
                case 0: return "███████"; break;
                case 1: return "██     "; break;
                case 2: return "█████  "; break;
                case 3: return "██     "; break;
                case 4: return "███████"; break;
            }
            break;

        case 'F':
            switch (index)
            {
                case 0: return "███████"; break;
                case 1: return "██     "; break;
                case 2: return "█████  "; break;
                case 3: return "██     "; break;
                case 4: return "██     "; break;
            }
            break;

        case 'G':
            switch (index)
            {
                case 0: return " ██████ "; break;
                case 1: return "██      "; break;
                case 2: return "██   ███"; break;
                case 3: return "██    ██"; break;
                case 4: return " ██████ "; break;
            }
            break;

        case 'H':
            switch (index)
            {
                case 0: return "██   ██"; break;
                case 1: return "██   ██"; break;
                case 2: return "███████"; break;
                case 3: return "██   ██"; break;
                case 4: return "██   ██"; break;
            }
            break;

        case 'I':
            switch (index)
            {
                case 0: return "██"; break;
                case 1: return "██"; break;
                case 2: return "██"; break;
                case 3: return "██"; break;
                case 4: return "██"; break;
            }
            break;

        case 'J':
            switch (index)
            {
                case 0: return "     ██"; break;
                case 1: return "     ██"; break;
                case 2: return "     ██"; break;
                case 3: return "██   ██"; break;
                case 4: return " █████ "; break;
            }
            break;

        case 'K':
            switch (index)
            {
                case 0: return "██   ██"; break;
                case 1: return "██  ██ "; break;
                case 2: return "█████  "; break;
                case 3: return "██  ██ "; break;
                case 4: return "██   ██"; break;
            }
            break;

        case 'L':
            switch (index)
            {
                case 0: return "██     "; break;
                case 1: return "██     "; break;
                case 2: return "██     "; break;
                case 3: return "██     "; break;
                case 4: return "███████"; break;
            }
            break;

        case 'M':
            switch (index)
            {
                case 0: return "███    ███"; break;
                case 1: return "████  ████"; break;
                case 2: return "██ ████ ██"; break;
                case 3: return "██  ██  ██"; break;
                case 4: return "██      ██"; break;
            }
            break;

        case 'N':
            switch (index)
            {
                case 0: return "███    ██"; break;
                case 1: return "████   ██"; break;
                case 2: return "██ ██  ██"; break;
                case 3: return "██  ██ ██"; break;
                case 4: return "██   ████"; break;
            }
            break;

        case 'O':
            switch (index)
            {
                case 0: return " ██████ "; break;
                case 1: return "██    ██"; break;
                case 2: return "██    ██"; break;
                case 3: return "██    ██"; break;
                case 4: return " ██████ "; break;
            }
            break;

        case 'P':
            switch (index)
            {
                case 0: return "██████ "; break;
                case 1: return "██   ██"; break;
                case 2: return "██████ "; break;
                case 3: return "██     "; break;
                case 4: return "██     "; break;
            }
            break;

        case 'Q':
            switch (index)
            {
                case 0: return " ██████ "; break;
                case 1: return "██    ██"; break;
                case 2: return "██ ▄▄ ██"; break;
                case 3: return " ██████ "; break;
                case 4: return "    ██  "; break;
            }
            break;

        case 'R':
            switch (index)
            {
                case 0: return "██████ "; break;
                case 1: return "██   ██"; break;
                case 2: return "██████ "; break;
                case 3: return "██   ██"; break;
                case 4: return "██   ██"; break;
            }
            break;

        case 'S':
            switch (index)
            {
                case 0: return "███████"; break;
                case 1: return "██     "; break;
                case 2: return "███████"; break;
                case 3: return "     ██"; break;
                case 4: return "███████"; break;
            }
            break;

        case 'T':
            switch (index)
            {
                case 0: return "████████"; break;
                case 1: return "   ██   "; break;
                case 2: return "   ██   "; break;
                case 3: return "   ██   "; break;
                case 4: return "   ██   "; break;
            }
            break;

        case 'U':
            switch (index)
            {
                case 0: return "██    ██"; break;
                case 1: return "██    ██"; break;
                case 2: return "██    ██"; break;
                case 3: return "██    ██"; break;
                case 4: return " ██████ "; break;
            }
            break;

        case 'V':
            switch (index)
            {
                case 0: return "██    ██"; break;
                case 1: return "██    ██"; break;
                case 2: return "██    ██"; break;
                case 3: return " ██  ██ "; break;
                case 4: return "  ████  "; break;
            }
            break;

        case 'W':
            switch (index)
            {
                case 0: return "██     ██"; break;
                case 1: return "██     ██"; break;
                case 2: return "██  █  ██"; break;
                case 3: return "██ ███ ██"; break;
                case 4: return " ███ ███ "; break;
            }
            break;

        case 'X':
            switch (index)
            {
                case 0: return "██   ██"; break;
                case 1: return " ██ ██ "; break;
                case 2: return "  ███  "; break;
                case 3: return " ██ ██ "; break;
                case 4: return "██   ██"; break;
            }
            break;

        case 'Y':
            switch (index)
            {
                case 0: return "██    ██"; break;
                case 1: return " ██  ██ "; break;
                case 2: return "  ████  "; break;
                case 3: return "   ██   "; break;
                case 4: return "   ██   "; break;
            }
            break;

        case 'Z':
            switch (index)
            {
                case 0: return "███████"; break;
                case 1: return "   ███ "; break;
                case 2: return "  ███  "; break;
                case 3: return " ███   "; break;
                case 4: return "███████"; break;
            }
            break;

        case ' ':
            switch (index)
            {
                case 0: return "  "; break;
                case 1: return "  "; break;
                case 2: return "  "; break;
                case 3: return "  "; break;
                case 4: return "  "; break;
            }
            break;

        case '-':
            switch (index)
            {
                case 0: return "      "; break;
                case 1: return "      "; break;
                case 2: return "██████"; break;
                case 3: return "      "; break;
                case 4: return "      "; break;
            }
            break;

        case '+':
            switch (index)
            {
                case 0: return "      "; break;
                case 1: return "  ██  "; break;
                case 2: return "██████"; break;
                case 3: return "  ██  "; break;
                case 4: return "      "; break;
            }
            break;

        case '!':
            switch (index)
            {
                case 0: return "██"; break;
                case 1: return "██"; break;
                case 2: return "██"; break;
                case 3: return "  "; break;
                case 4: return "██"; break;
            }
            break;

        case ',':
            switch (index)
            {
                case 0: return "  "; break;
                case 1: return "  "; break;
                case 2: return "  "; break;
                case 3: return "  "; break;
                case 4: return "▄█"; break;
            }
            break;

        case '\'':
            switch (index)
            {
                case 0: return "▄█"; break;
                case 1: return "  "; break;
                case 2: return "  "; break;
                case 3: return "  "; break;
                case 4: return "  "; break;
            }
            break;

        case '.':
            switch (index)
            {
                case 0: return "  "; break;
                case 1: return "  "; break;
                case 2: return "  "; break;
                case 3: return "  "; break;
                case 4: return "██"; break;
            }
            break;

        case '0':
            switch (index)
            {
                case 0: return " ██████ "; break;
                case 1: return "██  ████"; break;
                case 2: return "██ ██ ██"; break;
                case 3: return "████  ██"; break;
                case 4: return " ██████ "; break;
            }
            break;
        case '1':

            switch (index)
            {
                case 0: return " ██"; break;
                case 1: return "███"; break;
                case 2: return " ██"; break;
                case 3: return " ██"; break;
                case 4: return " ██"; break;
            }
            break;

        case '2':
            switch (index)
            {
                case 0: return "██████ "; break;
                case 1: return "     ██"; break;
                case 2: return " █████ "; break;
                case 3: return "██     "; break;
                case 4: return "███████"; break;
            }
            break;

        case '3':
            switch (index)
            {
                case 0: return "██████ "; break;
                case 1: return "     ██"; break;
                case 2: return " █████ "; break;
                case 3: return "     ██"; break;
                case 4: return "██████ "; break;
            }
            break;

        case '4':
            switch (index)
            {
                case 0: return "██   ██"; break;
                case 1: return "██   ██"; break;
                case 2: return "███████"; break;
                case 3: return "     ██"; break;
                case 4: return "     ██"; break;
            }
            break;

        case '5':
            switch (index)
            {
                case 0: return "███████"; break;
                case 1: return "██     "; break;
                case 2: return "███████"; break;
                case 3: return "     ██"; break;
                case 4: return "███████"; break;
            }
            break;

        case '6':
            switch (index)
            {
                case 0: return " ██████ "; break;
                case 1: return "██      "; break;
                case 2: return "███████ "; break;
                case 3: return "██    ██"; break;
                case 4: return " ██████ "; break;
            }
            break;

        case '7':
            switch (index)
            {
                case 0: return "███████"; break;
                case 1: return "     ██"; break;
                case 2: return "    ██ "; break;
                case 3: return "   ██  "; break;
                case 4: return "   ██  "; break;
            }
            break;

        case '8':
            switch (index)
            {
                case 0: return " █████ "; break;
                case 1: return "██   ██"; break;
                case 2: return " █████ "; break;
                case 3: return "██   ██"; break;
                case 4: return " █████ "; break;
            }
            break;

        case '9':
            switch (index)
            {
                case 0: return " █████ "; break;
                case 1: return "██   ██"; break;
                case 2: return " ██████"; break;
                case 3: return "     ██"; break;
                case 4: return " █████ "; break;
            }
            break;

        case '#':
            switch (index)
            {

                case 0: return " ██  ██ "; break;
                case 1: return "████████"; break;
                case 2: return " ██  ██ "; break;
                case 3: return "████████"; break;
                case 4: return " ██  ██ "; break;
            }
            break;

        case '%':
            switch (index)
            {

                case 0: return "██  ██"; break;
                case 1: return "   ██ "; break;
                case 2: return "      "; break;
                case 3: return " ██   "; break;
                case 4: return "██  ██"; break;
            }
            break;

        case '&':
            switch (index)
            {

                case 0: return "   ██   "; break;
                case 1: return "   ██   "; break;
                case 2: return "████████"; break;
                case 3: return "██  ██  "; break;
                case 4: return "██████  "; break;
            }
            break;

        case '/':
            switch (index)
            {

                case 0: return "    ██"; break;
                case 1: return "   ██ "; break;
                case 2: return "  ██  "; break;
                case 3: return " ██   "; break;
                case 4: return "██    "; break;
            }
            break;

        case '\\':
            switch (index)
            {

                case 0: return "██    "; break;
                case 1: return " ██   "; break;
                case 2: return "  ██  "; break;
                case 3: return "   ██ "; break;
                case 4: return "    ██"; break;
            }
            break;

        case '(':
            switch (index)
            {

                case 0: return " ██"; break;
                case 1: return "██ "; break;
                case 2: return "██ "; break;
                case 3: return "██ "; break;
                case 4: return " ██"; break;
            }
            break;

        case ')':
            switch (index)
            {

                case 0: return "██  "; break;
                case 1: return " ██ "; break;
                case 2: return " ██ "; break;
                case 3: return " ██ "; break;
                case 4: return "██  "; break;
            }
            break;

        case '{':
            switch (index)
            {

                case 0: return "  ██"; break;
                case 1: return " ██ "; break;
                case 2: return "█   "; break;
                case 3: return " ██ "; break;
                case 4: return "  ██"; break;
            }
            break;

        case '}':
            switch (index)
            {

                case 0: return "██  "; break;
                case 1: return " ██ "; break;
                case 2: return "   █"; break;
                case 3: return " ██ "; break;
                case 4: return "██  "; break;
            }
            break;

        case '[':
            switch (index)
            {

                case 0: return "███"; break;
                case 1: return "██ "; break;
                case 2: return "██ "; break;
                case 3: return "██ "; break;
                case 4: return "███"; break;
            }
            break;

        case ']':
            switch (index)
            {

                case 0: return "███"; break;
                case 1: return " ██"; break;
                case 2: return " ██"; break;
                case 3: return " ██"; break;
                case 4: return "███"; break;
            }
            break;

        case '?':
            switch (index)
            {

                case 0: return "██████ "; break;
                case 1: return "     ██"; break;
                case 2: return "  ▄███ "; break;
                case 3: return "  ▀▀   "; break;
                case 4: return "  ██   "; break;
            }
            break;

        case '*':
            switch (index)
            {

                case 0: return "      "; break;
                case 1: return "▄ ██ ▄"; break;
                case 2: return " ████ "; break;
                case 3: return "▀ ██ ▀"; break;
                case 4: return "      "; break;
            }
            break;

        case '>':
            switch (index)
            {

                case 0: return "    "; break;
                case 1: return "██  "; break;
                case 2: return "  ██"; break;
                case 3: return "██  "; break;
                case 4: return "   "; break;
            }
            break;

        case '<':
            switch (index)
            {

                case 0: return "     "; break;
                case 1: return "  ██ "; break;
                case 2: return "██   "; break;
                case 3: return "  ██ "; break;
                case 4: return "     "; break;
            }
            break;

        case ';':
            switch (index)
            {

                case 0: return "  "; break;
                case 1: return "██"; break;
                case 2: return "  "; break;
                case 3: return "▄█"; break;
                case 4: return "▀ "; break;
            }
            break;

        case ':':
            switch (index)
            {

                case 0: return "  "; break;
                case 1: return "██"; break;
                case 2: return "  "; break;
                case 3: return "██"; break;
                case 4: return "  "; break;
            }
            break;

        case '_':
            switch (index)
            {

                case 0: return "       "; break;
                case 1: return "       "; break;
                case 2: return "       "; break;
                case 3: return "       "; break;
                case 4: return "███████"; break;
            }
            break;

        case '@':
            switch (index)
            {

                case 0: return " ██████ "; break;
                case 1: return "██    ██"; break;
                case 2: return "██ ██ ██"; break;
                case 3: return "██ ██ ██"; break;
                case 4: return " █ ████ "; break;
            }
            break;

        case '$':
            switch (index)
            {

                case 0: return "▄▄███▄▄"; break;
                case 1: return "██     "; break;
                case 2: return "███████"; break;
                case 3: return "     ██"; break;
                case 4: return "▀▀███▀▀"; break;
            }
            break;
}
}
