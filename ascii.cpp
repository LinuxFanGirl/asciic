/*
       _   , __
    \_|_) /|/  \
      |    | __/
     _|    |   \
    (/\___/|(__/

    Lora Brekke
    ASCII Library - 13.11.2022
//TODO Add multiline support from both user input and a pipe output
*/
//Imports
#include <stdio.h>
#include <iostream>
#include <string>
#include "charlib.cpp"

//Std
using namespace std;

//Main Function that starts first
int main(int argc, char* argv[])
{
   string str = "";
   for (int i = 1; i < argc; ++i) { //Scans through every argument printed from the name of the command to all words typed
      str += argv[i]; //Because argv starts at 1 in order to get the whole message, it will add on more of the words
      str += " ";
   }
   if (argc < 2) { //If argc has only one argument (./ascii), check if a pipe output is pushed
      cin >> str;
   }
   //Starts the drawing process by looping from left to right and descending
    for (int y=0; y<5; y++) {
        for (int x=0; x<str.length(); x++) {
            cout << drawchar(str.at(x), y) << " "; //The current item is drawn here with the letter and its index of the characher
        }
        cout << "\n";
        //The whole sequence repeats from here
    }
return 0;
}
