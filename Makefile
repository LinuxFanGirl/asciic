
ascii: ascii.o charlib.o
	g++ ascii.o -o ascii

ascii.o: ascii.cpp
	g++ -c ascii.cpp

charlib.o: charlib.cpp
	g++ -c charlib.cpp

clean:
	rm *.o ascii

install:
	cp ascii /usr/local/bin/ascii

uninstall:
	rm /usr/local/bin/ascii
