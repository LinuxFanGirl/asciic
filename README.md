# ASCIIc
![](https://gitlab.com/LinuxFanGirl/asciic/-/raw/4270f19cc29e4985656face08c0bd42c1c135156/hello-world.png)

Welcome to my first program in C++! I'm very new to C++ but have experience in Java and Python. I thought I would program a heck of a way to write hello world by using ASCII block characters.
Feel free to try it out, view the source code, and modify for necessary changes for my need of improvement.

## Prequesites
Do ensure you have GCC installed your system before proceeding as we use the C++ compiler, g++.

Gentoo:
```
sudo emerge -q gcc
```
Fedora/Ultramarine:
```
sudo dnf install gcc
```
Debian/Ubuntu:
```
sudo apt install gcc
```
Arch/Artix Linux:
```
sudo pacman -S gcc
```
Void Linux:
```
sudo xbps-install -S gcc
```
Alpine Linux:
```
doas apk add gcc
```
FreeBSD (asciic is untested on FreeBSD):
```
sudo pkg install -y gcc
```

## Installation Process
Instructions for installation:

```
git clone https://gitlab.com/LinuxFanGirl/asciic.git
cd asciic
make
sudo make install
```
## Usage
The usage is pretty simple:
```
ascii Hello world!
```
or
```
ascii "Hello world!"
```
You can also pipe an output and push it to ASCII:
```
cat /etc/hostname | ascii
```
Please note that using it this way currently does not support multiple lines at the moment and not all characters are supported yet.

Limit to the english alphabet with some punctuation and math symbols.
